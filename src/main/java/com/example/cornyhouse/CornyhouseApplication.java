package com.example.cornyhouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CornyhouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CornyhouseApplication.class, args);
    }

}
