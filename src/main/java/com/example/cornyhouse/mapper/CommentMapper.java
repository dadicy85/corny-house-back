package com.example.cornyhouse.mapper;

import com.example.cornyhouse.dto.CommentDTO;
import com.example.cornyhouse.entity.Comment;

import java.util.stream.Collectors;

public class CommentMapper {

    public static CommentDTO toDTO(Comment comment) {
        if (comment == null) {
            return null;
        }
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setUsername(comment.getUser() != null ? comment.getUser().getUsername() : "Utilisateur inconnu");
        commentDTO.setCreatedAt(comment.getCreatedAt());
        commentDTO.setArticleId(comment.getArticle().getId());
        commentDTO.setReplies(comment.getReplies().stream()
                .map(CommentMapper::toDTO)
                .collect(Collectors.toList()));
        return commentDTO;
    }

    public static Comment toEntity(CommentDTO commentDTO) {
        if (commentDTO == null) {
            return null;
        }
        Comment comment = new Comment();
        comment.setId(commentDTO.getId());
        comment.setContent(commentDTO.getContent());
        comment.setCreatedAt(commentDTO.getCreatedAt());
        return comment;
    }
}
