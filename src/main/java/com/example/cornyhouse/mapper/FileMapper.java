package com.example.cornyhouse.mapper;

import com.example.cornyhouse.dto.FileDTO;
import com.example.cornyhouse.entity.File;

public class FileMapper {

    public static FileDTO toDTO(File file) {
        if (file == null) {
            return null;
        }

        FileDTO fileDTO = new FileDTO();
        fileDTO.setId(file.getId());
        fileDTO.setName(file.getName());
        fileDTO.setImageUrl(file.getImageUrl());
        fileDTO.setUsername(file.getUser().getUsername());

        return fileDTO;
    }

    public static File toEntity(FileDTO fileDTO) {
        if (fileDTO == null) {
            return null;
        }

        File file = new File();
        file.setId(fileDTO.getId());
        file.setName(fileDTO.getName());
        file.setImageUrl(fileDTO.getImageUrl());

        return file;
    }
}
