package com.example.cornyhouse.mapper;

import com.example.cornyhouse.dto.ArticleDTO;
import com.example.cornyhouse.entity.Article;

public class ArticleMapper {

    public static ArticleDTO toDTO(Article article) {
        if (article == null) {
            return null;
        }

        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(article.getId());
        articleDTO.setTitle(article.getTitle());
        articleDTO.setContent(article.getContent());
        articleDTO.setUser(UserMapper.toDTO(article.getUser()));
        articleDTO.setCreatedAt(article.getCreatedAt());
        articleDTO.setTheme(article.getTheme());
        return articleDTO;
    }

    public static Article toEntity(ArticleDTO articleDTO) {
        if (articleDTO == null) {
            return null;
        }

        Article article = new Article();
        article.setId(articleDTO.getId());
        article.setTitle(articleDTO.getTitle());
        article.setContent(articleDTO.getContent());
        article.setUser(UserMapper.toEntity(articleDTO.getUser()));
        article.setCreatedAt(articleDTO.getCreatedAt());
        article.setTheme(articleDTO.getTheme());
        return article;
    }
}
