package com.example.cornyhouse.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "menus")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    // N'est pas mappé dans la base de données car calculé dynamiquement
    @Transient
    private double totalPrice; // Prix total du menu

    // N'est pas mappé dans la base de données
    @Transient
    private String description;

    @Column(name = "student_discount")
    private double studentDiscount;

    @Column(name = "image_url")
    private String imageUrl;

    @ManyToMany
    @JoinTable(
            name = "menu_products",
            joinColumns = @JoinColumn(name = "menu_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products;
}