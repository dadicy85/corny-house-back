package com.example.cornyhouse.controller;

import com.example.cornyhouse.entity.Commande;
import com.example.cornyhouse.service.CommandeService;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/commandes")
public class CommandeController {

    private final CommandeService commandeService;
    private List<Commande> commandes = new ArrayList<>();
    private int nextId = 1;

    public CommandeController(CommandeService commandeService) {
        this.commandeService = commandeService;
    }

    @PostMapping
    public ResponseEntity<Long> addCommande(@RequestBody Commande commande, HttpSession session) {
        // Extrait l'identifiant de l'utilisateur de la session
        Long userId = (Long) session.getAttribute("userId");
        // Définit l'identifiant de l'utilisateur dans la commande
        commande.setId(userId);
        // Valide les données de la commande
        if (commande.getName() == null || commande.getEmail() == null ||
                commande.getDate() == null || commande.getTime() == null) {
            return ResponseEntity.badRequest().build();
        }
        // Ajoute la commande avec l'identifiant de l'utilisateur
        Long commandeId = commandeService.addCommande(commande).getId();
        return ResponseEntity.status(HttpStatus.CREATED).body(commandeId);
    }
}

