package com.example.cornyhouse.controller;

import com.example.cornyhouse.entity.Menu;
import com.example.cornyhouse.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/menus")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @GetMapping
    public List<Menu> getAllMenus() {
        return menuService.getAllMenus();
    }

    @GetMapping("/{menuId}")
    public Menu getMenuById(@PathVariable int menuId) {
        return menuService.getMenuById((long) menuId);
    }

    @PostMapping
    public Menu addMenu(@RequestBody Menu menu) {
        return menuService.addMenu(menu);
    }

    @PutMapping("/{menuId}")
    public Menu updateMenu(@PathVariable int menuId, @RequestBody Menu menu) {
        return menuService.updateMenu((long) menuId, menu);
    }

    @DeleteMapping("/{menuId}")
    public void deleteMenu(@PathVariable int menuId) {
        menuService.deleteMenu((long) menuId);
    }
}
