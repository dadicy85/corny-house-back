package com.example.cornyhouse.config;

import com.example.cornyhouse.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    @Autowired
    public JwtAuthenticationFilter(JwtService jwtService, UserDetailsService userDetailsService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        // Extraire le jeton JWT de la requête
        String jwtToken = extractJwtToken(request);

        // Valide le jeton JWT et extraire les rôles s'il est valide
        if (StringUtils.hasText(jwtToken) && jwtService.validateToken(jwtToken)) {
            List<String> roles = extractRolesFromToken(jwtToken);

            // Créer une authentication à partir des rôles
            Authentication authentication = jwtService.getAuthentication(jwtToken, roles);

            // Définit l'authentication dans le contexte de sécurité
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        // Passe la requête au filtre suivant
        filterChain.doFilter(request, response);
    }

    public List<String> extractRolesFromToken(String token) {
        try {
            // Extrait les revendications du jeton
            Jws<Claims> claimsJwt = Jwts.parser()
                    .setSigningKey(jwtService.getSecretKey())
                    .build()
                    .parseSignedClaims(token);

            // Extrait les rôles des revendications du jeton
            Claims claims = claimsJwt.getPayload();
            return claims.get("roles", List.class);
        } catch (Exception e) {
            // Gérer l'exception, par exemple en journalisant l'erreur
            e.printStackTrace();
            return null;
        }
    }

    private String extractJwtToken(HttpServletRequest request) {
        // Extrait le jeton JWT de l'en-tête Authorization
        String bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
