package com.example.cornyhouse.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ArticleDTO {
    private Long id;
    private String title;
    private String content;
    private UserDTO user;
    private LocalDateTime createdAt;
    private String theme;
    private List<CommentDTO> comments = new ArrayList<>();
}
