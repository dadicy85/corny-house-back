package com.example.cornyhouse.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CommentDTO {
    private Long id;
    private String content;
    private UserDTO user;
    private LocalDateTime createdAt;
    private Long articleId;
    private Long parentId;
    private String username;
    private List<CommentDTO> replies = new ArrayList<>();
}
