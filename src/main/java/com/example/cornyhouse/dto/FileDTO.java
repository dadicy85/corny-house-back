package com.example.cornyhouse.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileDTO {
    private Long id;
    private String name;
    private String imageUrl;
    private String username;
}
