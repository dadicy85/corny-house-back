package com.example.cornyhouse.service;

import com.example.cornyhouse.dto.FileDTO;
import com.example.cornyhouse.entity.File;
import com.example.cornyhouse.exception.ResourceNotFoundException;
import com.example.cornyhouse.mapper.FileMapper;
import com.example.cornyhouse.repository.FileRepository;
import com.example.cornyhouse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final FileRepository fileRepository;
    private final UserRepository userRepository;

    public FileStorageService(@Value("${file.upload-dir}") String uploadDir, FileRepository fileRepository, UserRepository userRepository) {
        this.fileStorageLocation = Paths.get(uploadDir).toAbsolutePath().normalize();
        this.fileRepository = fileRepository;
        this.userRepository = userRepository;

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new RuntimeException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public FileDTO store(MultipartFile file, String username) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new RuntimeException("Filename contains invalid path sequence " + fileName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            File fileEntity = new File();
            fileEntity.setName(fileName);
            fileEntity.setImageUrl(fileName); // Store relative path
            fileEntity.setUser(userRepository.findByUsername(username)
                    .orElseThrow(() -> new ResourceNotFoundException("Utilisateur introuvable")));

            fileRepository.save(fileEntity);

            return FileMapper.toDTO(fileEntity);
        } catch (IOException ex) {
            throw new RuntimeException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new RuntimeException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new RuntimeException("File not found " + fileName, ex);
        }
    }

    public List<FileDTO> getAllFiles() {
        return fileRepository.findAll().stream()
                .map(FileMapper::toDTO)
                .collect(Collectors.toList());
    }

    public void deleteFile(Long id) {
        File file = fileRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("File not found with id " + id));
        Path filePath = this.fileStorageLocation.resolve(file.getImageUrl()).normalize();
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException ex) {
            throw new RuntimeException("Could not delete file " + filePath.toString(), ex);
        }
        fileRepository.delete(file);
    }
}
