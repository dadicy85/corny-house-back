package com.example.cornyhouse.service;

import com.example.cornyhouse.dto.UserDTO;
import com.example.cornyhouse.entity.Role;
import com.example.cornyhouse.entity.User;
import com.example.cornyhouse.mapper.UserMapper;
import com.example.cornyhouse.repository.RoleRepository;
import com.example.cornyhouse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    // Récupérer tous les utilisateurs
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll().stream()
                .map(UserMapper::toDTO)
                .collect(Collectors.toList());
    }

    // Récupérer un utilisateur par son ID
    public UserDTO getUserById(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found"));
        return UserMapper.toDTO(user);
    }

    public User registerUser(User user) {
        // Vérifier si l'utilisateur existe déjà
        if (userRepository.findByEmail(user.getEmail()) != null) {
            // L'utilisateur existe déjà, renvoyer une option vide
            throw new RuntimeException("User with email " + user.getEmail() + " already exists");
        }

        // Encoder le mot de passe avant de l'enregistrer
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // Attribue le rôle "USER" à l'utilisateur (par défaut)
        Role userRole = roleRepository.findByName("USER");
        if (userRole == null) {
            // Gérer le cas où le rôle "USER" n'existe pas en base de données
            throw new RuntimeException("Role 'USER' not found in the database");
        }
        user.setRoles((List<Role>) Collections.singletonList(userRole));

        // Enregistre l'utilisateur dans la base de données
        return userRepository.save(user);
    }

    // Connecte un utilisateur
    public Optional<User> connectUser(String email, String password) {
        // Utilisation d'Optional pour rendre explicite la possibilité d'une valeur absente
        return Optional.ofNullable(userRepository.findByEmail(email))
                .filter(user -> passwordEncoder.matches(password, user.getPassword()));
    }

    // Met à jour un utilisateur
    public Optional<User> updateUser(Long id, User user) {
        // Utilisation d'Optional pour rendre explicite la possibilité d'une valeur absente
        return userRepository.existsById(id) ? Optional.of(userRepository.save(user)) : Optional.empty();
    }

    // Supprime un utilisateur
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    // Rôles d'un utilisateur
    public List<String> getUserRoles(Optional<User> user) {
        // Utilisation d'Optional pour traiter le cas où l'utilisateur est null
        return user.map(u -> u.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                .orElseThrow(() -> new UsernameNotFoundException("Utilisateur introuvable"));
    }

    public UserDTO findByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new RuntimeException("User not found"));
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setLastname(user.getLastname());
        userDTO.setRoles(user.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList()));
        return userDTO;
    }
}
