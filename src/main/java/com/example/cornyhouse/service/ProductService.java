package com.example.cornyhouse.service;

import com.example.cornyhouse.entity.Product;
import com.example.cornyhouse.entity.ProductType;
import com.example.cornyhouse.repository.ProductRepository;
import com.example.cornyhouse.repository.ProductTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private ProductTypeRepository productTypeRepository;

    public ProductService(ProductRepository productRepository, ProductTypeRepository productTypeRepository) {
        this.productRepository = productRepository;
        this.productTypeRepository = productTypeRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Produit non trouvé avec l'ID : " + productId));
    }

    public Product getProductByType(String productType) {
        Optional<ProductType> type = productTypeRepository.findByName(productType);
        return productRepository.findByTypeId(type.get().getId())
                .orElseThrow(() -> new IllegalArgumentException("Produit non trouvé avec le type : " + productType));
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Long productId, Product product) {
        if (!productRepository.existsById(productId)) {
            throw new IllegalArgumentException("Produit non trouvé avec l'ID : " + productId);
        }
        product.setId((long) productId);
        return productRepository.save(product);
    }

    public void deleteProduct(Long productId) {
        if (!productRepository.existsById(productId)) {
            throw new IllegalArgumentException("Produit non trouvé avec l'ID : " + productId);
        }
        productRepository.deleteById(productId);
    }
}
