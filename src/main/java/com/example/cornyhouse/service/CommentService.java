package com.example.cornyhouse.service;

import com.example.cornyhouse.dto.CommentDTO;
import com.example.cornyhouse.entity.Comment;
import com.example.cornyhouse.entity.User;
import com.example.cornyhouse.exception.ResourceNotFoundException;
import com.example.cornyhouse.mapper.CommentMapper;
import com.example.cornyhouse.repository.ArticleRepository;
import com.example.cornyhouse.repository.CommentRepository;
import com.example.cornyhouse.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final ArticleRepository articleRepository;
    private final UserRepository userRepository;

    public CommentService(CommentRepository commentRepository, ArticleRepository articleRepository, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.articleRepository = articleRepository;
        this.userRepository = userRepository;
    }

    public List<CommentDTO> getAllComments() {
        return commentRepository.findAll().stream()
                .map(CommentMapper::toDTO)
                .collect(Collectors.toList());
    }

    public CommentDTO getCommentById(Long id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Commentaire introuvable avec l'ID : " + id));
        return CommentMapper.toDTO(comment);
    }

    public List<CommentDTO> getCommentsByArticleId(Long articleId) {
        return commentRepository.findByArticleId(articleId).stream()
                .map(CommentMapper::toDTO)
                .collect(Collectors.toList());
    }

    public CommentDTO createComment(CommentDTO commentDTO) {
        Comment comment = CommentMapper.toEntity(commentDTO);
        comment.setUser(userRepository.findByUsername(commentDTO.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("Utilisateur introuvable")));
        comment.setArticle(articleRepository.findById(commentDTO.getArticleId())
                .orElseThrow(() -> new ResourceNotFoundException("Article introuvable")));
        comment = commentRepository.save(comment);
        return CommentMapper.toDTO(comment);
    }

    public CommentDTO updateComment(Long id, CommentDTO commentDTO) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Commentaire introuvable avec l'ID : " + id));
        comment.setContent(commentDTO.getContent());
        comment.setUser(userRepository.findByUsername(commentDTO.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("Utilisateur introuvable")));
        comment.setArticle(articleRepository.findById(commentDTO.getArticleId())
                .orElseThrow(() -> new ResourceNotFoundException("Article introuvable")));
        comment = commentRepository.save(comment);
        return CommentMapper.toDTO(comment);
    }

    public void deleteComment(Long id) {
        if (!commentRepository.existsById(id)) {
            throw new ResourceNotFoundException("Commentaire introuvable avec l'ID : " + id);
        }
        commentRepository.deleteById(id);
    }
}
