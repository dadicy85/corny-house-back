package com.example.cornyhouse.service;

import com.example.cornyhouse.dto.ArticleDTO;
import com.example.cornyhouse.entity.Article;
import com.example.cornyhouse.exception.ResourceNotFoundException;
import com.example.cornyhouse.mapper.ArticleMapper;
import com.example.cornyhouse.mapper.UserMapper;
import com.example.cornyhouse.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public List<ArticleDTO> getAllArticles() {
        return articleRepository.findAll().stream()
                .map(ArticleMapper::toDTO)
                .collect(Collectors.toList());
    }

    public ArticleDTO getArticleById(Long id) {
        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Article introuvable"));
        return ArticleMapper.toDTO(article);
    }

    public ArticleDTO getArticleByTitle(String title) {
        Article article = articleRepository.findByTitle(title)
                .orElseThrow(() -> new RuntimeException("Article introuvable"));
        return ArticleMapper.toDTO(article);
    }

    public ArticleDTO createArticle(ArticleDTO articleDTO) {
        Article article = ArticleMapper.toEntity(articleDTO);
        article = articleRepository.save(article);
        return ArticleMapper.toDTO(article);
    }

    public ArticleDTO updateArticle(Long id, ArticleDTO articleDTO) {
        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Article introuvable"));
        article.setTitle(articleDTO.getTitle());
        article.setContent(articleDTO.getContent());
        article.setUser(UserMapper.toEntity(articleDTO.getUser()));
        article = articleRepository.save(article);
        return ArticleMapper.toDTO(article);
    }

    public void deleteArticle(Long id) {
        articleRepository.deleteById(id);
    }
}
