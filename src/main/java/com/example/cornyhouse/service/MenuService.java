package com.example.cornyhouse.service;

import com.example.cornyhouse.entity.Menu;
import com.example.cornyhouse.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

    private MenuRepository menuRepository;
    private ProductService productService;

    public MenuService(MenuRepository menuRepository, ProductService productService) {
        this.menuRepository = menuRepository;
        this.productService = productService;
    }

    public List<Menu> getAllMenus() {
        return menuRepository.findAll();
    }

    public Menu getMenuById(Long menuId) {
        return menuRepository.findById(menuId)
                .orElseThrow(() -> new IllegalArgumentException("Menu not found with ID: " + menuId));
    }

    public Menu addMenu(Menu menu) {
        return menuRepository.save(menu);
    }

    public Menu updateMenu(Long menuId, Menu menu) {
        if (!menuRepository.existsById(menuId)) {
            throw new IllegalArgumentException("Menu not found with ID: " + menuId);
        }
        return menuRepository.save(menu);
    }

    public void deleteMenu(Long menuId) {
        if (!menuRepository.existsById(menuId)) {
            throw new IllegalArgumentException("Menu not found with ID: " + menuId);
        }
        menuRepository.deleteById(menuId);
    }
}
